<?php

class Ship
{
    const VERTICAL   = 'v';
    const HORIZONTAL = 'h';

    private $size;
    private $coordinates;
    private $timesHit = 0;

    public function __construct($coordinates, $size)
    {
        $this->coordinates = $coordinates;
        $this->size = $size;
    }

    public function getCoordinates(){
        return $this->coordinates;
    }

    public function hit(){
        $this->timesHit++;
    }

    public function isSunk(){
        return $this->size == $this->timesHit;
    }
}