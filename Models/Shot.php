<?php

class Shot
{
    private $coordinates;
    private $status;

    public function __construct(Coordinates $coordinates, $status)
    {
        $this->coordinates = $coordinates;
        $this->status = $status;
    }

    public function getCoordinates(){
        return $this->coordinates;
    }

    public function getStatus(){
        return $this->status;
    }
}