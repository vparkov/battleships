<?php

class Coordinates
{
    private $x;
    private $y;

    public function __construct($x, $y)
    {
        if(is_int($x) && is_int($y)){
            $this->x = $x;
            $this->y = $y;
        }
    }

    public function getX(){
        return $this->x;
    }

    public function getY(){
        return $this->y;
    }
}