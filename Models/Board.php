<?php

class Board
{
    const MAX_ATTEMPTS = 1000;

    private $rows;
    private $cols;

    private $ships = [];//array of Ship objects
    private $shots = [];//array of Shot objects

    private $board;//dummy board used when spawning the ships

    public function __construct($config)
    {
        $this->rows = $config['boardRows'];
        $this->cols = $config['boardCols'];
        $this->populateBoard($config['ships']);
    }

    //returns random unoccupied field
    private function getRandomEmptyField(){
        $attempts = 0;
        while($attempts++ < Board::MAX_ATTEMPTS){
            $x = rand(1, $this->rows);
            $y = rand(1, $this->cols);
            if($this->board[$x][$y] == 0){
                return ['x' => $x, 'y' => $y];
            }
        }
        throw new Exception('Couldn\'t create ship. No empty fields.');
    }

    //returns all the ships
    public function getShips(){
        return $this->ships;
    }

    public function getShots(){
        return $this->shots;
    }

    public function getRows(){
        return $this->rows;
    }

    public function getCols(){
        return $this->cols;
    }

    public function addShot($shot){
        $this->shots[] = $shot;
    }

    //extract field status from the dummy board
    private function updateFieldStatus($coordinates, $value){
        $this->board[$coordinates['x']][$coordinates['y']] = $value;
    }


    //fills the board with ships
    private function populateBoard($ships){
        if(empty($ships)){
            throw new Exception('Didn\'t find any ships to create');
        }

        //creating dummy board so it is easier to check if ships overlap
        for($i = 1;  $i <= $this->rows; $i++){
            for($j = 1;  $j <= $this->cols; $j++){
                $this->board[$i][$j] = 0;
            }
        }

        foreach($ships as $shipType){
            for($i = 0; $i < $shipType['amount']; $i++){
                $this->createShip($shipType['length']);
            }
        }
    }

    //Try and create the ship
    private function createShip($shipSize){
        $attempts = 0;
        while($attempts++ < Board::MAX_ATTEMPTS) {

            $position = $this->getRandomEmptyField();
            $orientation = rand(0,1) == 1 ? Ship::HORIZONTAL : Ship::VERTICAL;
            $coordinates = $this->canCreateShip($position, $orientation, $shipSize);

            if(!empty($coordinates)){
                $this->ships[] = new Ship($this->createCoordinateObjects($coordinates), $shipSize);
                foreach ($coordinates as $coordinate){
                    $this->updateFieldStatus($coordinate, 1);
                }
                return true;
            }
        }

        throw new Exception('Couldn\'t create ship. I tried too many times. Maybe the board is full.');
    }

    //checks if can create ship
    private function canCreateShip($position, $orientation, $size){
        $shipCoordinates = $this->generateShipCoordinates($position, $orientation, $size);
        foreach ($shipCoordinates as $coordinates){
            //outside the board
            if($coordinates['x'] > $this->rows || $coordinates['y'] > $this->cols){
                return [];
            }
            //already occupied coordinates
            if($this->board[$coordinates['x']][$coordinates['y']] != 0){
                return [];
            }
        }
        return $shipCoordinates;
    }

    //create the ship coordinates given starting position, orientation and size
    private function generateShipCoordinates($startingPosition, $orientation, $size){
        $x = $startingPosition['x'];
        $y = $startingPosition['y'];
        $coordinates = [];

        if($orientation == 'h') {
            for($i = $y; $i < $y + $size; $i++){
                $coordinates[] = ['x' => $x, 'y' => $i];
            }
        } else { //vertical
            for($i = $x; $i < $x + $size; $i++) {
                $coordinates[] = ['x' => $i, 'y' => $y];
            }
        }
        return $coordinates;
    }

    private function createCoordinateObjects($coordinates){
        $preparedCoordinates = [];
        foreach($coordinates as $coords){
            $preparedCoordinates[] = new Coordinates($coords['x'], $coords['y']);
        }
        return $preparedCoordinates;
    }

    //hit ship and return true if you sink it
    public function hitShipAt($coordinates){
        foreach($this->ships as $ship){
            if(in_array($coordinates, $ship->getCoordinates())){
                $ship->hit();
                if($ship->isSunk()){
                    return true;
                }
            }
        }
        return false;
    }
}