<?php


class ConsoleGame extends Game
{
    public function __construct()
    {
        parent::__construct();
    }

    public function play(){
        $display = new Display($this->getBoard(), $this->getMessage(), $this->isGameOver());
        $display->printGame(false);

        $handle = fopen("php://stdin","r");

        while(!$this->isGameOver()) {

            $coords = trim(fgets($handle));

            $hint = ($coords === 'show');
            if (isset($coords) && $coords != '' && !$hint) {
                $this->attack($coords);
            }

            $display->updateGame($this->getBoard(), $this->getMessage(), $this->isGameOver());
            $display->printGame($hint);
        }
    }
}