<?php

abstract class Game
{
    protected $board;
    private $message;

    public function __construct($gameState = []) {

        if(empty($gameState)){
            //new Game
            $this->board = new Board($this->loadConfig());
        } else {
            //resume game from state
            $this->board = unserialize($gameState);
        }
    }

    abstract protected function play();

    private function loadConfig() {

        $configPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.json';
        $configData = json_decode(file_get_contents($configPath), true);
        if($configData == NULL || !isset($configData['battleShips'])) {
            throw new Exception('Config file is broken!');
        }
        return $configData['battleShips'];
    }

    public function getBoard(){
        return $this->board;
    }

    public function getMessage(){
        return $this->message;
    }

    private function setMessage($message){
        $this->message = "*** " . $message . " ***";
    }

    public function attack($input){
        if (preg_match('/^([a-jA-J])([1-9]|10)$/', $input)) {
            $x = ord(strtolower(substr($input, 0, 1))) - 96;
            $y = (int)substr($input, 1);

            $coordinates = new Coordinates($x, $y);
            $this->setMessage("Miss");

            foreach ($this->board->getShots() as $shot) {
                $shotCoords = $shot->getCoordinates();
                if ($shotCoords == $coordinates) {
                    $this->setMessage("Already Hit");
                    return false;
                }
            }

            foreach ($this->board->getShips() as $ship) {
                if (in_array($coordinates, $ship->getCoordinates())) {
                    $this->setMessage("Hit");
                    $this->board->addShot(new Shot($coordinates, true));

                    if($this->board->hitShipAt($coordinates)){
                        $this->setMessage("Sunk");
                    }
                    return true;
                }
            }

            $this->board->addShot(new Shot($coordinates, false));
        } else {
            $this->setMessage("Error");
        }
        return false;
    }

    public function isGameOver(){
        foreach($this->board->getShips() as $ship) {
            if(!$ship->isSunk()){
                return false;
            }
        }
        return true;
    }
}