<?php


class WebGame extends Game
{
//    private $battleShips;
    private $hint;
    private $attackCoords;

    public function __construct()
    {
        session_start();
        //gets game state from the session
        $gameState = isset($_SESSION['game']) ? $_SESSION['game'] : [];
        parent::__construct($gameState);

        $this->hint = (isset($_POST['coords']) && $_POST['coords'] === 'show');
        $this->attackCoords = isset($_POST['coords']) ? $_POST['coords'] : "";
    }

    public function play(){
        if(!$this->hint && !empty($this->attackCoords)){
            $this->attack($this->attackCoords);
        }

        if($this->isGameOver()){
            session_destroy();
        }

        $display = new Display($this->getBoard(), $this->getMessage(), $this->isGameOver());
        $display->printGame($this->hint);
        $this->saveGameState();
    }

    private function saveGameState()
    {
        //storing the game state in the session
        $_SESSION['game'] = serialize($this->getBoard());
    }


}