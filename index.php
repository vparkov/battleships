<?php

require_once 'AutoLoader.php';
// 
try{
    $game = php_sapi_name() === 'cli' ? new ConsoleGame() :  new WebGame();
    $game->play();
} catch(Exception $e) {
    echo $e->getMessage();
}
