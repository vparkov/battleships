<?php

class Display
{

    private $board;
    private $message;
    private $gameOver;
    private $hint = false;


    public function __construct(Board $board, $message, $gameOver){
        $this->board = $board;
        $this->message = $message;
        $this->gameOver = $gameOver;
    }

    public function updateGame(Board $board, $message, $gameOver){
        $this->board = $board;
        $this->message = $message;
        $this->gameOver = $gameOver;
    }

    public function printGame($hint){
        $this->hint = $hint;
        php_sapi_name() === 'cli' ? $this->printConsoleGame() : $this->printWebGame();
    }

    private function printConsoleGame(){

        passthru(DIRECTORY_SEPARATOR == '/' ? 'clear' : 'cls'); // detect if linux or windows
        $this->printBoard();

        if($this->gameOver){
            echo ' Well done! You completed the game in ' . count($this->board->getShots()) . " shots\n";
        } else {
            echo "\n\nEnter coordinates (row, col), e.g. A5: ";
        }
    }

    private function printWebGame(){

        echo '<pre>';
        $this->printBoard();
        echo '</pre>';

        if($this->gameOver){
            echo 'Well done! You completed the game in ' . count($this->board->getShots()) . " shots<br><br>";
            echo '<a href="index.php">Play again?</a>';
        } else {

            echo '<form method="post" action="index.php">
                  Enter coordinates (row, col), e.g. A5 <input type="text" size="5" name="coords" autofocus autocomplete="off">
                  <input type="submit" value="Submit">
                  <br>
              </form>';
        }
    }

    private function printBoard(){
        $printableBoard = $this->prepareBoard();

        echo $this->message . "\n\n" . " ";
        for($i = 1; $i <= $this->board->getCols(); $i++){
            echo ' ' . $i . ' ';
        }
        echo "\n";
        foreach ($printableBoard as $index => $rows){
            echo chr(64 + $index);
            foreach($rows as $field){
                echo " " . $field . " ";
            }
            echo "\n";
        }
        echo "\n";
    }

    private function prepareBoard(){
        $ships = $this->board->getShips();
        $shots = $this->board->getShots();

        for($i = 1; $i <= $this->board->getRows(); $i++){
            for($j = 1; $j <= $this->board->getCols(); $j++){
                $printableBoard[$i][$j] = $this->hint ? ' ' : '.';
            }
        }

        if($this->hint){
            foreach($ships as $ship){
                foreach($ship->getCoordinates() as $coordinates){
                    $x = $coordinates->getX();
                    $y = $coordinates->getY();
                    $printableBoard[$x][$y] = 'X';
                }
            }
        }

        foreach($shots as $shot){
            $x = $shot->getCoordinates()->getX();
            $y = $shot->getCoordinates()->getY();
            $shotStatus= $this->hint ? ' ' : ($shot->getStatus() ? 'X' : '-');
            $printableBoard[$x][$y] = $shotStatus;
        }

        if(!empty($printableBoard)){
            return $printableBoard;
        } else {
            throw new Exception("Failed when creating the board");
        }
    }
}